package de.syranda.spidermysql.utils;

import java.io.File;
import java.io.IOException;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import de.syranda.spidermysql.plugin.Main;
import de.syranda.spidermysql.plugin.ConfigValues;

public class Config {

	private static File configFile;
	private static FileConfiguration config;
	
	public static boolean loadConfig(Main c) {
		
		configFile = new File(c.getDataFolder(), "//config.yml");
		config = YamlConfiguration.loadConfiguration(configFile);
	
		config.addDefault("Config.LogQuery", false);
		
		config.addDefault("MySQL.MainConnection.Host", "127.0.0.1");
		config.addDefault("MySQL.MainConnection.Port", 3306);
		config.addDefault("MySQL.MainConnection.Database", "database");
		config.addDefault("MySQL.MainConnection.User", "root");
		config.addDefault("MySQL.MainConnection.Password", "");
			
		ConfigValues.LOG_QUERY = config.getBoolean("Config.LogQuery");
		
		ConfigValues.MYSQL_HOST = config.getString("MySQL.MainConnection.Host");
		ConfigValues.MYSQL_PORT = config.getInt("MySQL.MainConnection.Port");
		ConfigValues.MYSQL_DATABASE = config.getString("MySQL.MainConnection.Database");
		ConfigValues.MYSQL_USER = config.getString("MySQL.MainConnection.User");
		ConfigValues.MYSQL_PASSWORD = config.getString("MySQL.MainConnection.Password");
		
		if(!configFile.exists()) {
		
			config.addDefault("MySQL.BackupConnections.Element_1.Host", "127.0.0.1");
			config.addDefault("MySQL.BackupConnections.Element_1.Port", 3306);
			config.addDefault("MySQL.BackupConnections.Element_1.Database", "backup_database");
			config.addDefault("MySQL.BackupConnections.Element_1.User", "root");
			config.addDefault("MySQL.BackupConnections.Element_1.Password", "");
			
		}
		
		config.options().copyDefaults(true);
		try {
			config.save(configFile);
		} catch (IOException e) {
			return false;
		}
		
		return true;
		
	}
	
	public static File getConfigFile() {
		
		return configFile;
		
	}
	
	public static FileConfiguration getConfig() {
		
		return config;	
		
	}
	
}
