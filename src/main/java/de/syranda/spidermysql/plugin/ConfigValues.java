package de.syranda.spidermysql.plugin;

public class ConfigValues {

	public static boolean LOG_QUERY;
	
	public static String MYSQL_HOST;
	public static int MYSQL_PORT;
	public static String MYSQL_DATABASE;
	public static String MYSQL_USER;
	public static String MYSQL_PASSWORD;
	
}
