package de.syranda.spidermysql.plugin;

import org.bukkit.plugin.java.JavaPlugin;

import de.syranda.spidermysql.customclasses.ConnectionManager;
import de.syranda.spidermysql.customclasses.helper.DefaultClassSerializers;
import de.syranda.spidermysql.utils.Config;

public class Main extends JavaPlugin {

	private static Main main;
	
	@Override
	public void onEnable() {
		
		main = this;
		
		if(Config.loadConfig(this))	{
			System.out.println("[SpiderMySQL] Connecting to MySQL-Server...");
			
			if(ConnectionManager.connect(ConfigValues.MYSQL_HOST, ConfigValues.MYSQL_PORT, ConfigValues.MYSQL_DATABASE, ConfigValues.MYSQL_USER, ConfigValues.MYSQL_PASSWORD))
				System.out.println("[SpiderMySQL] Connected to MySQL-Server.");
		
			if(Config.getConfig().isConfigurationSection("MySQL.BackupConnections")) {
				
				for(String path : Config.getConfig().getConfigurationSection("MySQL.BackupConnections").getKeys(false)) {
					
					if(!path.startsWith("Element_"))
						continue;
					
					int number = Integer.parseInt(path.split("_")[1]);
					
					String host = Config.getConfig().getString("MySQL.BackupConnections." + path + ".Host");
					int port = Config.getConfig().getInt("MySQL.BackupConnections." + path + ".Port");
					String database = Config.getConfig().getString("MySQL.BackupConnections." + path + ".Database");
					String user = Config.getConfig().getString("MySQL.BackupConnections." + path + ".User");
					String password = Config.getConfig().getString("MySQL.BackupConnections." + path + ".Password");
					
					System.out.println("[SpiderMySQL] Connecting to backup MySQL-Server (# " + number + ")...");
					
					if(ConnectionManager.addBackupConnection(host, port, database, user, password))
						System.out.println("[SpiderMySQL] Connected to backup MySQL-Server (# " + number + ").");
					
				}
			
			}
			
		} else
			System.err.println("[SpiderMySQL] Error loading config.");
	
		DefaultClassSerializers.loadDefaultClassSerializers();
		
	}
		
	@Override
	public void onDisable() {

		ConnectionManager.disconnect();

	}
	
	public static Main getInstance() {
		
		return main;
		
	}
	
}
