package de.syranda.spidermysql.customclasses;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.HashMap;
import java.util.Map.Entry;

import org.json.simple.JSONObject;

import de.syranda.spidermysql.customclasses.builder.ColumnType;
import de.syranda.spidermysql.customclasses.builder.Exclude;
import de.syranda.spidermysql.customclasses.builder.ExcludeField;
import de.syranda.spidermysql.customclasses.builder.ForeignKey;
import de.syranda.spidermysql.customclasses.builder.Key;
import de.syranda.spidermysql.customclasses.builder.MySQLClass;
import de.syranda.spidermysql.customclasses.table.ClassSerializer;
import de.syranda.spidermysql.customclasses.table.MySQLField;
import de.syranda.spidermysql.customclasses.table.TableClass;
import de.syranda.spidermysql.utils.Serializer;

public class MySQLTableSyntax {
	
	private String tableName;
	private String database;
	
	public MySQLTableSyntax(String tableName) {
		
		this.tableName = tableName;

	}
	
	public MySQLTableSyntax(String tableName, String database) {

		this.tableName = tableName;
		this.database = database;

	}
	
	public MySQLTableSyntax(TableClass table) {
		
		this.tableName = table.getTableName();
		this.database = table.getDatabase();
		
	}
	
	public String insert(String values) {
		
		if(!checkSyntax(values))
			ConnectionManager.handleException(new IllegalArgumentException("Syntax error for query " + values), false);
		
		String query = "INSERT INTO " + getTarget() + " (" + getEscapedKeyList(values, ",") +  ") VALUES (" + getEscapedValueList(values, ",") + ")";
		
		return query;
			
	}

	public String insertUpdate(String values) {
		
		if(!checkSyntax(values))
			ConnectionManager.handleException(new IllegalArgumentException("Syntax error for query " + values), false);
		
		String query = "INSERT INTO " + getTarget() + " (" + getEscapedKeyList(values, ",") +  ") VALUES (" + getEscapedValueList(values, ",") + ") ON DUPLICATE KEY UPDATE " + getEscapedKeyValueList(values, ",");
		
		return query;
			
	}
	
	public String insertIgnore(String values) {
		
		if(!checkSyntax(values))
			ConnectionManager.handleException(new IllegalArgumentException("Syntax error for query " + values), false);
		
		String query = "INSERT IGNORE INTO " + getTarget() + " (" + getEscapedKeyList(values, ",") +  ") VALUES (" + getEscapedValueList(values, ",") + ")";
		
		return query;
			
	}
	
	public String insert(Object clazz, String add) {
				
		return insert(getSpiderList(clazz, add));
		
	}

	public String insert(Object clazz) { 
	
		return insert(clazz, "");
		
	}
	
	public String insertUpdate(Object clazz, String add) {
			
		return insertUpdate(getSpiderList(clazz, add));
			
	}
	
	public String insertUpdate(Object clazz) {

		return insertUpdate(clazz, "");
	
	}
	
	public String insertIgnore(Object clazz, String add) {
					
		return insertIgnore(getSpiderList(clazz, add));
		
	}
	
	public String insertIgnore(Object clazz) {

		return insertIgnore(clazz, "");		
		
	}
	
	
	public String get(String what, String where) {
		
		if(!checkListSyntax(what) && !what.equalsIgnoreCase("*"))
			throw new IllegalArgumentException("Syntax error for query " + what);
		
		if(!checkSyntax(where))
			throw new IllegalArgumentException("Syntax error for query " + where);
		
		String query = "SELECT " + getEscapedKeyList(what, ",") + " FROM " + getTarget() + " WHERE " + getEscapedKeyValueList(where, "AND");
		
		return query;
		
	}
	
	public String get(String where) {
		
		if(!checkSyntax(where))
			throw new IllegalArgumentException("Syntax error for query " + where);
		
		String query = "SELECT * FROM " + getTarget() + " WHERE " + getEscapedKeyValueList(where, "AND");
		
		return query;
		
	}
	
	public String update(String what, String where) {
		
		if(!checkSyntax(what))
			throw new IllegalArgumentException("Syntax error for query " + what);
		
		if(!checkSyntax(where) && !where.equals(""))
			throw new IllegalArgumentException("Syntax error for query " + where);
		
		String query = "UPDATE " + getTarget() + " SET " + getEscapedKeyValueList(what, ",");
		
		if(!where.equals(""))
			query += " WHERE " + getEscapedKeyValueList(where, "AND");
		
		return query;
		
	}
	
	public String update(String what) {
		
		if(!checkSyntax(what))
			throw new IllegalArgumentException("Syntax error for query " + what);
		
		return update(what, "");
		
	}
	
	public String remove(String where) {
		
		if(!checkSyntax(where))
			throw new IllegalArgumentException("Syntax error for query " + where);
		
		String query = "DELETE FROM " + getTarget() + " WHERE " + getEscapedKeyValueList(where, "AND");
		
		return query;		
		
	}
	
	public String get() {
		
		return "SELECT * FROM " + getTarget() + "";
		
	}
	
	public String clear() {
		
		String query = "TRUNCATE " + getTarget() + "";
		
		return query;
		
	}
	
	public String getTarget() {

		return database == null ? "`" + tableName + "`" : "`" + database + "`.`" + tableName + "`";
		
	}
	
	public String getDatabase() {
		
		return this.database;
		
	}

	public String changeColumn(String field, MySQLField newField) {
		
		String query = "ALTER TABLE " + getTarget() + " CHANGE COLUMN `" + field + "` `" + newField.getFieldName() + "` " + newField.getType() + (newField.getLength() != 0 ? "(" + newField.getLength() + ")" : "");
		
		return query;
		
	}
	
	public String setColumnDefault(String field, Object newDefault) {
		
		String query = "ALTER TABLE " + getTarget() + " ALTER COLUMN `" + field + "`"; 
		
		if(newDefault == null)
			query += " DROP DEFAULT";
		else {
			
			int defaultValue;
			
			try {
			
				defaultValue = Integer.parseInt(newDefault.toString());
				query += " SET DEFAULT " + defaultValue;
				
			} catch (Exception e) {
				
				query += " SET DEFAULT '" + newDefault.toString() + "'";
				
			}
			
		}
		
		return query;
		
	}
	
	public String changeColumnType(MySQLField field) {
	
		String query = "ALTER TABLE " + getTarget() + " MODIFY COLUMN `" + field.getFieldName() + "` " + field.getType() + (field.getLength() != 0 ? "(" + field.getLength() + ")" : "");
		
		if(field.isAutoIncrement())
			query += " AUTO_INCREMENT";
		
		return query;
		
	}
	
	public String addField(MySQLField field) {
		
		String query = "ALTER TABLE " + getTarget() + " ADD COLUMN `" + field.getFieldName() + "` " + field.getType() + (field.getLength() != 0 ? "(" + field.getLength() + ")" : "");
		
		if(field.allowNull())
			query += " NOT NULL";
		if(field.getDefaultValue() != null) { 
			
			int defaultValue;
			
			try {
			
				defaultValue = Integer.parseInt(field.getDefaultValue().toString());
				query += " DEFAULT " + defaultValue;
				
			} catch (Exception e) {
				
				query += " DEFAULT '" + field.getDefaultValue().toString() + "'";
				
			}
			
		}
		
		if(field.isAutoIncrement())
			query += " AUTO_INCREMENT";
		
		return query;

	}

	public String removeColumn(String field) {

		String query = "ALTER TABLE " + getTarget() + " DROP COLUMN `" + field + "`";
		
		return query;
		
	}

	public String dropPrimaryKey() {
		
		String query = "ALTER TABLE " + getTarget() + " DROP PRIMARY KEY";
		
		return query;
		
	}
	
	public String dropKey(String key) {
		
		String query = "ALTER TABLE " + getTarget() + " DROP KEY `" + key + "`";
		
		return query;
		
	}
	
	public String dropForeignKey(String foreignKey) {
		
		String query = "ALTER TABLE " + getTarget() + " DROP FOREIGN KEY `" + foreignKey + "`";
		
		return query;
		
	}
	
	public String addKey(Key key) {
		
		String query = "ALTER TABLE " + getTarget() + " ADD ";
		
		String keys = "";
		
		for(int i = 0; i < key.getColumns().length; i++)
			keys += i == 0 ? "`" + key.getColumns()[i] + "`" : ", `" + key.getColumns()[i] + "`";
		
		switch (key.getKeyType()) {
		case PRIMARY:
			query += "PRIMARY KEY (" + keys + ")";
			break;
		case UNIQUE:
			query += "UNIQUE KEY `" + key.getKeyName() + "` (" + keys + ")";
			break;
		default:
			break;
		}
		
		return query;
		
	}
	
	public String addForeignKey(ForeignKey key) {
		
		String query = "ALTER TABLE " + getTarget() + " ADD ";
		
		String targetC = "";
		
		for(int ii = 0; ii < key.getTargetColumns().length; ii++)
			targetC += ii == 0 ? "`" + key.getTargetColumns()[ii] + "`" : ", `" + key.getTargetColumns()[ii] + "`";
		
		String referenceC = "";				
		
		for(int ii = 0; ii < key.getReferenceColumns().length; ii++)
			referenceC += ii == 0 ? "`" + key.getReferenceColumns()[ii] + "`" : ", `" + key.getReferenceColumns()[ii] + "`";
		
		query += "CONSTRAINT `" + key.getKeyName() + "` FOREIGN KEY (" + targetC + ") REFERENCES `" + key.getRefrenceTable() + "` (" + referenceC + ") ON DELETE " + key.getOnDelete().getAction() + " ON UPDATE " + key.getOnUpdate().getAction();
		
		return query;
		
	}
	
	public String delete() {
		
		String query = "DROP TABLE IF EXISTS " + getTarget();
		
		return query;
		
	}
	
	private static boolean checkListSyntax(String raw) {
		
		return raw.matches("^[^;]+(;[^;])*");
		
	}
	
	private static boolean checkSyntax(String raw) {
		
		return raw.matches("^[^;:]+:[^;]+(;[^;:]+:[^;]+)*");
		
	}
	
	private static String getEscapedKeyList(String raw, String connector) {
		
		String ret = "";
		
		for(int i = 0; i < raw.split(";").length; i++)
			ret += i == 0 ? "`" + raw.split(";")[i].split(":", 2)[0] + "`" : " " + connector + " " + "`" + raw.split(";")[i].split(":", 2)[0] + "`";
		
		if(ret.contains(">"))
			ret = ret.split(">")[0] + "`";
		
		return ret;
		
	}
	
	@SuppressWarnings("unchecked")
	private static String getEscapedValueList(String raw, String connector) {		
		
		String ret = "";
		
		HashMap<String, JSONObject> jsons = new HashMap<String, JSONObject>();
		
		for(int i = 0; i < raw.split(";").length; i++) {
			
			String key = raw.split(";")[i].split(":", 2)[0];
			String value = raw.split(";")[i].split(":", 2)[1];
			
			if(key.matches("[^>]+>[^>]+")) {
				
				String subkey = key.split(">")[0];
				
				JSONObject json = jsons.containsKey(subkey) ? jsons.get(subkey) : new JSONObject();
				json.put(key.split(">", 2)[1], value);
				
				jsons.put(subkey, json);
				
				continue;				
				
			}			
			
			String pre = "";
			
			if(value.matches("^(OLD|NEW)(\\.{1})\\w([^\\.]*)")) {
				pre = value.split("\\.")[0] + ".";
				value = value.split("\\.")[1];
				
				ret += i == 0 ? pre + "`" + value + "`" : " " + connector + " " + pre + "`" + value + "`";				
				continue;
			}
			
			int intValue;
					
			try {				
				intValue = Integer.parseInt(value);
				ret += i == 0 ? intValue : connector + intValue;				
			} catch(Exception e) {
				ret += i == 0 ? "'" + value + "'" : " " + connector + " " + "'" + value + "'";
			}			
					
		}
		
		for(Entry<String, JSONObject> json : jsons.entrySet())
			ret += ret.equals("") ? "'" + json.getValue().toJSONString() + "'" : " " + connector + " '" + json.getValue().toJSONString() + "'";
		
		return ret;
		
	}
	
	@SuppressWarnings("unchecked")
	private static String getEscapedKeyValueList(String raw, String connector) {
		
		String ret = "";
		
		HashMap<String, JSONObject> jsons = new HashMap<String, JSONObject>();
		
		for(int i = 0; i < raw.split(";").length; i++) {
			
			String key = raw.split(";")[i].split(":", 2)[0];			
			String value = raw.split(";")[i].split(":", 2)[1];	
			
			if(key.matches("[^>]+>[^>]+")) {
				
				String subkey = key.split(">")[0];
				
				JSONObject json = jsons.containsKey(subkey) ? jsons.get(subkey) : new JSONObject();
				json.put(key.split(">", 2)[1], value);
				
				jsons.put(subkey, json);
				
				continue;				
				
			}	
			
			String pre = "";
			
			if(value.matches("^(OLD|NEW)(\\.{1})\\w([^\\.]*)")) {
				pre = value.split("\\.")[0] + ".";
				value = value.split("\\.")[1];
				
				ret += i == 0 ? "`" + key + "`=" + pre + "`" + value + "`" : " " + connector + " `" + key + "`=" + pre + "`" + value + "`";				
				continue;
				
			}
			
			int intValue;
					
			try {				
				intValue = Integer.parseInt(value);
				ret += i == 0 ? "`" + key + "`=" + intValue : " " + connector + " " + "`" + key + "`=" + intValue;			
			} catch(Exception e) {
				ret += i == 0 ? "`" + key + "`='" + value + "'" : " " + connector + " " + "`" + key + "`='" + value + "'";
			}	
			
		}		
		
		for(Entry<String, JSONObject> json : jsons.entrySet())
			ret += ret.equals("") ? "`" + json.getKey() + "`='" + json.getValue().toJSONString() + "'" : " " + connector + " `" + json.getKey() + "`='" + json.getValue().toJSONString() + "'";
		
		return ret;
	
	}
	
	private String getSpiderList(Object clazz, String add) {
		
		String list = "";
		
		boolean first = true;
		boolean adjust = clazz.getClass().isAnnotationPresent(MySQLClass.class) ? clazz.getClass().getAnnotation(MySQLClass.class).adjustFields(): false;
		
		for(int i = 0; i < clazz.getClass().getDeclaredFields().length; i++) {
			
			Field field = clazz.getClass().getDeclaredFields()[i];
			field.setAccessible(true);
			
			if((!field.isAnnotationPresent(de.syranda.spidermysql.customclasses.builder.MySQLField.class) && !clazz.getClass().isAnnotationPresent(MySQLClass.class)) || field.isAnnotationPresent(Exclude.class) || Modifier.isStatic(field.getModifiers()))
				continue;
			
			String fieldName = adjust ? ConventionFormatter.getMySQLName(field.getName()) : field.getName();
			
			try {				
				if(ClassSerializer.hasSerializer(field.getType())) {
					
					for(int index = 0; index < ClassSerializer.getSerializer(field.getType()).serializeObject(fieldName, field.get(clazz)).size(); index++) {
						
						@SuppressWarnings("unchecked")
						Entry<String, Object> entry = (Entry<String, Object>) ClassSerializer.getSerializer(field.getType()).serializeObject(fieldName, field.get(clazz)).entrySet().toArray()[index];
						
						if(field.isAnnotationPresent(ExcludeField.class)) {
							
							boolean con = true;
							
							for(String exField : field.getAnnotation(ExcludeField.class).fields())
								if(entry.getKey().equals(exField))
									con = false;
						
							if(!con)
								continue;
							
						}
						
						if(first) {
							list += entry.getKey() + ":" + entry.getValue();
							first = false;
						} else							
							list += ";" + entry.getKey() + ":" + entry.getValue();		
						
					}
					
				} else if(field.getType().isEnum() && ColumnType.getType(field.getType()) == ColumnType.NONE)
					list += first ? fieldName + ":" + ((Enum<?>) field.get(clazz)).name() : ";" + fieldName + ":" + ((Enum<?>) field.get(clazz)).name();
				else if(Serializable.class.isAssignableFrom(field.getType()) && ColumnType.getType(field.getType()) == ColumnType.NONE)
					list += first ? fieldName + ":" + Serializer.serialize(field.get(clazz)) : ";" + fieldName + ":" + Serializer.serialize(field.get(clazz));
				else					
					list += first ? fieldName + ":" + field.get(clazz).toString() : ";" + fieldName + ":" + field.get(clazz).toString();					
					
			} catch (IllegalArgumentException e) {}
			catch (IllegalAccessException e) {}
			
			first = false;
			
		}
			
		if(!add.equals(""))
			list += ";" + add;
		
		return list;
		
	}
	
}
