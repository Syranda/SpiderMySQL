package de.syranda.spidermysql.customclasses.event;

public interface Schedule {

	public String getScheduleString();
	
	public String getIntervalString();
	
}
