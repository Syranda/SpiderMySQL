package de.syranda.spidermysql.customclasses.event;

import java.text.SimpleDateFormat;
import java.util.Date;

public class TimestampSchedule implements Schedule {

	private String scheduleString;
	
	public TimestampSchedule(long timestamp) {

		scheduleString = "'" + new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date(timestamp)) + "'";
		
	}
	
	public TimestampSchedule() {

		scheduleString = "'" + new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date(System.currentTimeMillis())) + "'";
		
	}
	
	public TimestampSchedule addInterval(int quantity, Interval interval) {

		scheduleString += " + INTERVAL " + quantity + " " + interval.name();
		
		return this;
		
	}
	
	public TimestampSchedule addInterval(String quantity, Interval interval) {

		scheduleString += " + INTERVAL '" + quantity + "' " + interval.name();
		
		return this;
		
	}
	
	@Override
	public String getScheduleString() {

		return "AT " + scheduleString;
		
	}
	
	@Override
	public String getIntervalString() {
		
		return scheduleString;
		
	}

}
