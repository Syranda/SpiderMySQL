package de.syranda.spidermysql.customclasses.event;

import de.syranda.spidermysql.customclasses.ConnectionManager;

public class EventBuilder {

	private String name;
	private Schedule schedule;	
	private String task = "";
	private boolean preserve = true;
	private boolean enabled = true; 
	
	public EventBuilder(String name) {
		
		this.name = name;
		
	}

	public EventBuilder setSchedule(Schedule schedule) {
		
		this.schedule = schedule;
		
		return this;
		
	}
	
	public EventBuilder addStatement(String statement) {
		
		task += statement + ";";
		
		return this;
		
	}
	
	public EventBuilder setPreserve(boolean preserve) {
		
		this.preserve = preserve;
		
		return this;
		
	}
	
	public EventBuilder setEnabled(boolean enabled) {
		
		this.enabled = enabled;
		
		return this;
		
	}
	
	public void build() {
		
		String query = "CREATE EVENT IF NOT EXISTS `" + name + "`"
				+ " ON SCHEDULE " + schedule.getScheduleString() 
				+ " ON COMPLETION" + (preserve ? "" : " NOT") 
				+ " PRESERVE " + (enabled ? "ENABLE" : "DISABLE")
				+ " DO BEGIN "
				+ task
				+ " END";
		
		ConnectionManager.insertStatement(query, true);
		
	}
	
}
