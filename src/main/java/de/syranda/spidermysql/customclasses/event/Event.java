package de.syranda.spidermysql.customclasses.event;

import de.syranda.spidermysql.customclasses.ConnectionManager;

public class Event {

	private String name;
	
	public Event(String name) {

		this.name = name;

	}
	
	public EventBuilder builder() {
		
		return new EventBuilder(name);
		
	}
	
	public void delete() {
		
		ConnectionManager.insertStatement("DROP EVENT IF EXISTS `" + name + "`", true);
		
	}
	
	public void setEnabled(boolean enable) {
		
		ConnectionManager.insertStatement("ALTER EVENT `" + name + "` " + (enable ? "ENABLE" : "DISABLE"), true);
		
	}

	public void setSchedule(Schedule schedule) {
		
		ConnectionManager.insertStatement("ALTER EVENT `" + name + "` ON SCHEDULE " + schedule.getScheduleString(), true);
		
	}
	
}
