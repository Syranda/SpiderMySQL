package de.syranda.spidermysql.customclasses.event;

public class IntervalSchedule implements Schedule {
	
	private String scheduleString;
	
	public IntervalSchedule(int quantity, Interval interval) {

		scheduleString = quantity + " " + interval.name();

	}

	public IntervalSchedule(String quantity, Interval interval) {

		scheduleString = "'" + quantity + "' " + interval.name();

	}
	
	public IntervalSchedule starts(TimestampSchedule schedule) {
		
		scheduleString += " STARTS " + schedule.getIntervalString();
		
		return this;
		
	}
	
	public IntervalSchedule ends(TimestampSchedule schedule) {
		
		scheduleString += " ENDS " + schedule.getIntervalString();
		
		return this;
		
	}
	
	@Override
	public String getScheduleString() {

		return "EVERY " + scheduleString;
		
	}
	
	@Override
	public String getIntervalString() {

		return scheduleString;
		
	}

}
