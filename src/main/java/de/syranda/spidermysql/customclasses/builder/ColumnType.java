package de.syranda.spidermysql.customclasses.builder;

import java.io.Serializable;

public enum ColumnType {

	INT("int", 0), LONG("bigint", 0), SHORT("smallint", 0), DOUBLE("double", 0), STRING("varchar", 255), BOOLEAN("tinyint", 0), NONE("NONE", 0), TEXT("text", 0);
	
	private String stringType;
	private int defaultLength;
	
	private ColumnType(String stringType, int defaultLength) {

		this.stringType = stringType;
		this.defaultLength = defaultLength;

	}
	
	public String getTypeName() {
		
		return this.stringType;
		
	}
	
	public int getDefaultLength() {
		
		return this.defaultLength;
		
	}

	public static ColumnType getType(Class<?> field) {
		
				
		
		switch(field.getSimpleName()) {
		case "int":
			return ColumnType.INT;
		case "long":
			return ColumnType.LONG;
		case "short":
			return ColumnType.SHORT;
		case "float":
			return ColumnType.DOUBLE;
		case "double":
			return ColumnType.DOUBLE;
		case "String":
			return ColumnType.STRING;
		case "boolean":
			return ColumnType.BOOLEAN;
		case "text":
			return ColumnType.TEXT;
		default:
			if(Serializable.class.isAssignableFrom(field))
				return ColumnType.TEXT;
			else
				return null;
		}
		
	}
	
}
