package de.syranda.spidermysql.customclasses.builder;

public enum ForeignKeyAction {

	NO_ACTION("NO ACTION"), CASCADE("CASCADE"), RESTRICT("RESTRICT"), SET_NULL("SET NULL");
	
	private String action;
	
	private ForeignKeyAction(String action) {
		
		this.action = action;
		
	}
	
	public String getAction() {
		
		return this.action;
		
	}
	
}
