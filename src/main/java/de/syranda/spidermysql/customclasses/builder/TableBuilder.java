package de.syranda.spidermysql.customclasses.builder;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

import com.mysql.jdbc.Connection;

import de.syranda.spidermysql.customclasses.ConnectionManager;
import de.syranda.spidermysql.customclasses.ConventionFormatter;
import de.syranda.spidermysql.customclasses.MySQLTableSyntax;
import de.syranda.spidermysql.customclasses.table.ClassSerializer;
import de.syranda.spidermysql.customclasses.table.MySQLField;
import de.syranda.spidermysql.customclasses.trigger.Trigger;
import de.syranda.spidermysql.customclasses.trigger.TriggerEvent;
import de.syranda.spidermysql.customclasses.trigger.TriggerTime;

public class TableBuilder {

	private String tableName;
	
	private List<MySQLField> fields = new ArrayList<MySQLField>();
	private List<Key> keys = new ArrayList<Key>();
	private List<ForeignKey> fKeys = new ArrayList<ForeignKey>();
	
	private String database;
	
	public TableBuilder(String tableName) {

		this.tableName = tableName;
		
	}
	
	public TableBuilder(String tableName, String database) {

		this.tableName = tableName;
		this.database = database;
		
	}
	
	public String getTableName() {
		
		return this.tableName;
		
	}
	
	public TableBuilder addField(MySQLField field) {
		
		fields.add(field);
		
		return this;
		
	}
	
	public TableBuilder addField(String fieldName, Class<?> field, String[] exclude) {
		
		ClassSerializer<?> serializer = ClassSerializer.getSerializer(field);
			
		TableBuilder ret = this;
		
		for(Entry<String, Class<?>> entry : serializer.getPattern(fieldName).entrySet()) {
						
			boolean con = true;
			
			if(exclude != null)
				for(String exField : exclude) 				
					if(entry.getKey().equals(exField)) {
						con = false;
						break;
					}				
			
			if(!con)
				continue;
			
			if(ClassSerializer.hasSerializer(entry.getValue())) 
				ret = addField(fieldName, entry.getValue(), exclude);
			else
				ret = addField(new MySQLField(entry.getKey(), ColumnType.getType(entry.getValue())));		
			
		}
		
		return ret;
		
	}
	
	public TableBuilder addForeignKey(ForeignKey key) {
		
		fKeys.add(key);
		
		return this;
		
	}
	
	public TableBuilder addKey(Key key) {
		
		keys.add(key);
		
		return this;
		
	}
	
	public TableBuilder addFieldsOfClass(Class<?> clazz) {
		
		HashMap<String, List<String>> keys = new HashMap<String, List<String>>();
		
		boolean adjust = clazz.isAnnotationPresent(MySQLClass.class) ? clazz.getAnnotation(MySQLClass.class).adjustFields(): false;
		
		for(Field field : clazz.getDeclaredFields()) {
			
			if((!field.isAnnotationPresent(de.syranda.spidermysql.customclasses.builder.MySQLField.class) && !clazz.isAnnotationPresent(MySQLClass.class)) || field.isAnnotationPresent(Exclude.class) || Modifier.isStatic(field.getModifiers()))
				continue;
			
			de.syranda.spidermysql.customclasses.builder.MySQLField ann = field.getAnnotation(de.syranda.spidermysql.customclasses.builder.MySQLField.class);			
			
			if(ClassSerializer.hasSerializer(field.getType())) {
				
				addField(adjust ? ConventionFormatter.getMySQLName(field.getName()) : field.getName(), field.getType(), field.isAnnotationPresent(ExcludeField.class) ? field.getAnnotation(ExcludeField.class).fields() : null);
				continue;
				
			}
			
			ColumnType type = ColumnType.getType(field.getType());		
			
			MySQLField mField = new MySQLField(adjust ? ConventionFormatter.getMySQLName(field.getName()) : field.getName(), type);

			mField.setLength(type.getDefaultLength());
			
			if(ann != null) {
			
				if(ann.type() != ColumnType.NONE)
					mField.setType(ann.type());
				if(ann.length() != 0)
					mField.setLength(ann.length());
				mField.setAllowNull(!ann.notNull());
				mField.setAutoIncrement(ann.autoIncrement());
				mField.setPrimaryKey(ann.isPrimaryKey());
				
			}
			
			if(mField.getType().equalsIgnoreCase("none"))
				continue;
			
			if(field.isAnnotationPresent(Primary.class))
				if(!keys.containsKey("primary"))
					keys.put("primary", Arrays.asList(field.getName()));
				else {
					
					List<String> list = keys.get("primary");
					list.add(field.getName());
					keys.put("primary", list);
					
				}
			
			if(field.isAnnotationPresent(Unique.class))	{				
				List<String> list = !keys.containsKey(field.getAnnotation(Unique.class).name()) ? new ArrayList<String>() : keys.get(field.getAnnotation(Unique.class).name());
				list.add(field.getName());
				keys.put(field.getAnnotation(Unique.class).name(), list);		
			}
			
			addField(mField);
			
		}
		
		for(String key : keys.keySet()) {		
			
			String[] fields = new String[keys.get(key).size()];
			for(int i = 0; i < fields.length; i++)
				fields[i] = keys.get(key).get(i);
			
			if(key.equals("primary")) 
				addKey(new Key("", KeyType.PRIMARY, fields));
			else
				addKey(new Key(key, KeyType.UNIQUE, fields));		
		
		}
			
		return this;
		
	}
	
	public void build() {
		
		String query = "CREATE TABLE IF NOT EXISTS " + getTarget() + " (";
		
		for(int i = 0; i < fields.size(); i++) {
			
			MySQLField field = fields.get(i);
			
			if(i != 0)
				query += ", ";
			
			query += "`" + field.getFieldName() + "` " + field.getType() + (field.getLength() != 0 ? "(" + field.getLength() + ")" : "");
			
			if(!field.allowNull())
				query += " NOT NULL";
			
			if(field.getDefaultValue() != null) { 
				
				int defaultValue;
				
				try {
				
					defaultValue = Integer.parseInt(field.getDefaultValue().toString());
					query += " DEFAULT " + defaultValue;
					
				} catch (Exception e) {
					
					query += " DEFAULT '" + field.getDefaultValue().toString() + "'";
					
				}
				
			}
			
			if(field.isAutoIncrement())
				query += " AUTO_INCREMENT";
			
			if(field.isPrimaryKey())
				query += " PRIMARY KEY";
			
		}
		
		if(!keys.isEmpty()) {				
			
			for(Key key : keys) {
				
				String keys = "";
				
				for(int i = 0; i < key.getColumns().length; i++)
					keys += i == 0 ? "`" + key.getColumns()[i] + "`" : ", `" + key.getColumns()[i] + "`";
				
				switch (key.getKeyType()) {
				case PRIMARY:
					query += ", PRIMARY KEY (" + keys + ")";
					break;
				case UNIQUE:
					query += ", UNIQUE KEY `" + key.getKeyName() + "` (" + keys + ")";
					break;
				default:
					break;
				}
				
			}
			
		}
		
		if(!fKeys.isEmpty()) {
			
			query += ", ";
			
			for(int i = 0; i < fKeys.size(); i++) {
				
				ForeignKey key = fKeys.get(i);
				
				if(i != 0)
					query += ", ";
				
				String targetC = "";
				
				for(int ii = 0; ii < key.getTargetColumns().length; ii++)
					targetC += ii == 0 ? "`" + key.getTargetColumns()[ii] + "`" : ", `" + key.getTargetColumns()[ii] + "`";
				
				String referenceC = "";				
				
				for(int ii = 0; ii < key.getReferenceColumns().length; ii++)
					referenceC += ii == 0 ? "`" + key.getReferenceColumns()[ii] + "`" : ", `" + key.getReferenceColumns()[ii] + "`";
				
				query += "CONSTRAINT `" + key.getKeyName() + "` FOREIGN KEY (" + targetC + ") REFERENCES `" + key.getRefrenceTable() + "` (" + referenceC + ") ON DELETE " + key.getOnDelete().getAction() + " ON UPDATE " + key.getOnUpdate().getAction();
				
			}
			
		}
		
		query += ") ENGINE=InnoDB";	
		
		ConnectionManager.insertStatement(query, true);
		
		for(Entry<String, Connection> connection : ConnectionManager.getBackupConnections().entrySet()) {
			
			String insertQuery = "";
			String updateQueryWhat = "";
			String updateQueryWhere = "";
			String deleteQuery = "";
			
			for(int i = 0; i < fields.size(); i++) {
				
				MySQLField field = fields.get(i);
			
				insertQuery += i == 0 ? field.getFieldName() + ":NEW." + field.getFieldName() : ";" + field.getFieldName() + ":NEW." + field.getFieldName(); 
				updateQueryWhat += i == 0 ? field.getFieldName() + ":NEW." + field.getFieldName(): ";" + field.getFieldName() + ":NEW." + field.getFieldName(); 
				updateQueryWhere += i == 0 ? field.getFieldName() + ":OLD." + field.getFieldName() : ";" + field.getFieldName() + ":OLD." + field.getFieldName(); 
				deleteQuery += i == 0 ? field.getFieldName() + ":OLD." + field.getFieldName() : ";" + field.getFieldName() + ":OLD." + field.getFieldName(); 
				
			}
			
			Trigger insertTrigger = new Trigger(connection.getKey() + "_" + tableName + "_insert");
			insertTrigger.builder(tableName, TriggerTime.AFTER, TriggerEvent.INSERT)
				.addStatement(new MySQLTableSyntax(tableName, connection.getKey()).insert(insertQuery))
			.build();
			
			Trigger updateTrigger = new Trigger(connection.getKey() + "_" + tableName + "_update");
			updateTrigger.builder(tableName, TriggerTime.AFTER, TriggerEvent.UPDATE)
				.addStatement(new MySQLTableSyntax(tableName, connection.getKey()).update(updateQueryWhat, updateQueryWhere))
			.build();
			
			Trigger deleteTrigger = new Trigger(connection.getKey() + "_" + tableName + "_delete");
			deleteTrigger.builder(tableName, TriggerTime.AFTER, TriggerEvent.DELETE)
				.addStatement(new MySQLTableSyntax(tableName, connection.getKey()).remove(deleteQuery))
			.build();
			
		}
		
	}

	public String getTarget() {

		return database == null ? "`" + tableName + "`" : "`" + database + "`.`" + tableName + "`";
		
	}
	
	public String getDatabase() {
		
		return this.database;
		
	}

	public List<MySQLField> getFields() {

		return this.fields;
		
	}
	
	public List<Key> getKeys() {
		
		return this.keys;
		
	}
	
	public List<ForeignKey> getForeignKeys() {
		
		return this.fKeys;
		
	}
	
}
