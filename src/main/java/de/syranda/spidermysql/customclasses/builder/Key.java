package de.syranda.spidermysql.customclasses.builder;

public class Key {

	private String name;
	private KeyType keyType;
	private String[] coloumns;
	
	public Key(String name, KeyType keyType, String... coloumns) {
		
		this.name = name;
		this.keyType = keyType;
		this.coloumns = coloumns;
		
	}
	
	public String getKeyName() {
		
		return this.name;
		
	}
	
	public KeyType getKeyType() {
		
		return keyType;
		
	}
	
	public String[] getColumns() {
		
		return this.coloumns;
		
	}
	
}
