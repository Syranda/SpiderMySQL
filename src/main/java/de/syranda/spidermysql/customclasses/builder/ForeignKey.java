package de.syranda.spidermysql.customclasses.builder;

public class ForeignKey {

	private String keyName;
	private String[] columns;
	private String reference;
	private String[] referenceColumns;
		
	private ForeignKeyAction onUpdate;
	private ForeignKeyAction onDelete;
	
	public ForeignKey(String keyName, String reference, ForeignKeyAction onUpdate, ForeignKeyAction onDelete) {

		this.keyName = keyName;
		this.reference = reference;
		
		this.onUpdate = onUpdate;
		this.onDelete = onDelete;
	
	}	
	
	public ForeignKey setTargetColumns(String... columns) {
		
		this.columns = columns;
		
		return this;
		
	}
	
	public ForeignKey setReferenceColumns(String... columns) {
		
		this.referenceColumns = columns;
	
		return this;
		
	}
	
	public String getKeyName() {
		
		return this.keyName;
		
	}

	public String getRefrenceTable() {
		
		return this.reference;
		
	}
	
	public String[] getTargetColumns() {
		
		return this.columns;
		
	}
	
	public String[] getReferenceColumns() {
		
		return this.referenceColumns;
		
	}
	
	public ForeignKeyAction getOnUpdate() {
		
		return this.onUpdate;
		
	}
	
	public ForeignKeyAction getOnDelete() {
		
		return this.onDelete;
		
	}
	
}
