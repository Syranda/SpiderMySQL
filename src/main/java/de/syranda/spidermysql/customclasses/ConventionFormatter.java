package de.syranda.spidermysql.customclasses;

import java.util.ArrayList;
import java.util.List;

public class ConventionFormatter {

	public static String getMySQLName(String name) {
		
		List<String> letters = new ArrayList<String>();
		
		for(int i = 0; i < name.length(); i++)
			if(Character.isUpperCase(name.charAt(i)) && i != 0 && !Character.isUpperCase(name.charAt(i - 1)))
				letters.add("_" + Character.toLowerCase(name.charAt(i)));
			else
				letters.add(Character.toLowerCase(name.charAt(i)) + "");
			
		String ret = "";
		
		for(String letter : letters)
			ret += letter;
			
		return ret;
		
	}
	
}
