package de.syranda.spidermysql.customclasses.trigger;

public enum TriggerEvent {

	
	INSERT("INSERT"), DELETE("DELETE"), UPDATE("UPDATE");
	
	private String syntax;
	
	private TriggerEvent(String syntax) {

		this.syntax = syntax;

	}
	
	public String getSyntax() {
		
		return this.syntax;
		
	}
	
}
