package de.syranda.spidermysql.customclasses.trigger;

public enum TriggerTime {

	BEFORE("BEFORE"), AFTER("AFTER");
	
	private String syntax;
	
	private TriggerTime(String syntax) {

		this.syntax = syntax;
	
	}
	
	public String getSyntax() {
		
		return this.syntax;
		
	}
	
}
