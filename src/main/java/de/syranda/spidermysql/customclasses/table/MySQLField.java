package de.syranda.spidermysql.customclasses.table;

import de.syranda.spidermysql.customclasses.builder.ColumnType;

public class MySQLField implements Cloneable {

	private String fieldName;
	private String type;
	private int length = 0;
	private Object defaultValue;
	private boolean allowNull = true;
	private boolean autoIncrement = false;
	private boolean isPrimaryKey = false;
	
	public MySQLField(String fieldName, String type) {
		
		this.fieldName = fieldName;
		this.type = type;
		
	}
	
	public MySQLField(String fieldName, ColumnType type) {
		
		this.fieldName = fieldName;
		this.type = type.getTypeName();
		this.length = type.getDefaultLength();
		
	}
	
	public String getFieldName() {
		
		return this.fieldName;
		
	}
	
	public String getType() {
		
		return this.type;
		
	}
	
	public int getLength() {
		
		return this.length;
		
	}
	
	public Object getDefaultValue() {
		
		return this.defaultValue;
		
	}
	
	public boolean allowNull() {
		
		return this.allowNull;
		
	}	

	public boolean isAutoIncrement() {
		return autoIncrement;
	}

	
	public MySQLField setLength(int length) {
		
		this.length = length;
		
		return this;
		
	}
	
	public MySQLField setLength(Object object) {

		int length;
		
		try {
			
			length = Integer.parseInt(object.toString());
			this.length = length;
			
		} catch (Exception e) {}
		
		return this;
				
	}		
	
	public MySQLField setDefaultValue(Object defaultValue) {
		
		this.defaultValue = defaultValue;
		
		return this;
		
	}
	
	public MySQLField setAllowNull(boolean allowNull) {
		
		this.allowNull = allowNull;
		
		return this;
		
	}

	public MySQLField setAutoIncrement(boolean autoIncrement) {
		
		this.autoIncrement = autoIncrement;
		
		return this;
		
	}
	
	@Override
	public Object clone() throws CloneNotSupportedException {
		return super.clone();
	}

	public MySQLField setType(ColumnType type) {

		this.type = type.getTypeName();
		
		return this;
		
	}


	public MySQLField setType(String type) {

		this.type = type;
		
		return this;
		
	}
	
	public MySQLField setPrimaryKey(boolean isPrimaryKey) {
		
		this.isPrimaryKey = isPrimaryKey;
		
		return this;
		
	}
	
	public boolean isPrimaryKey() {
		
		return this.isPrimaryKey;
		
	}
	
}
