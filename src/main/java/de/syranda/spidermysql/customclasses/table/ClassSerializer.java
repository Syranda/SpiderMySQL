package de.syranda.spidermysql.customclasses.table;

import java.util.HashMap;

public abstract class ClassSerializer<N> {

	private static HashMap<Class<?>, ClassSerializer<?>> serializers = new HashMap<Class<?>, ClassSerializer<?>>();
	
	public static void addClassSerializer(Class<?> clazz, ClassSerializer<?> serializer) {
		
		serializers.put(clazz, serializer);
		
	}
	
	public static boolean hasSerializer(Class<?> clazz) {
		
		return serializers.containsKey(clazz);
		
	}
	
	public static ClassSerializer<?> getSerializer(Class<?> clazz) {
		
		return serializers.get(clazz);
		
	}
	
	@SuppressWarnings("unchecked")
	public final HashMap<String, Object> serializeObject(String fieldName, Object object) {
		
		return serialize(fieldName, (N) object);
		
	}
	
	public abstract HashMap<String, Object> serialize(String fieldName, N object);
	
	public abstract N deserialize(String fieldName, HashMap<String, Object> columns);

	public abstract HashMap<String, Class<?>> getPattern(String fieldName);
	
}
