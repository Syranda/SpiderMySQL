package de.syranda.spidermysql.customclasses.table;

import java.io.Serializable;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Modifier;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map.Entry;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import de.syranda.spidermysql.customclasses.ConnectionManager;
import de.syranda.spidermysql.customclasses.ConventionFormatter;
import de.syranda.spidermysql.customclasses.builder.ColumnType;
import de.syranda.spidermysql.customclasses.builder.Exclude;
import de.syranda.spidermysql.customclasses.builder.ExcludeField;
import de.syranda.spidermysql.customclasses.builder.MySQLClass;
import de.syranda.spidermysql.utils.Serializer;

public class RecordResult {

	private ResultSet rs;
	private HashMap<String, Object[]> results = new HashMap<String, Object[]>();
	private HashMap<Class<?>, Object[]> resultsClasses = new HashMap<Class<?>, Object[]>();
	private int size = 0;
	private int index = -1;	
	
	public RecordResult(ResultSet rs) {
		
		this.rs = rs;		
		
		try {
			rs.beforeFirst();
			while(rs.next())
				size++;	
			
			rs.beforeFirst();			
		} catch (SQLException e) {}	
				
	}
	
	public Object[] getObject(String col) {
		
		Object[] res = results.get(col);
		
		if(res == null) {
			
			res = new Object[getSize()];
			
			try {
				
				rs.beforeFirst();
				
				int index = 0;
				while(rs.next()) {
					res[index] = rs.getObject(col).toString().equalsIgnoreCase("null") ? null : rs.getObject(col);
					index++;
				}
					
				rs.beforeFirst();
					
			} catch (Exception e) {}
			
			results.put(col, res);
			
			
		}
		
		return res;
		
	}
	
	public Object getFirstObject(String col) {
		
		return getObject(col)[0];
		
	}
	
	public boolean[] getBoolean(String col) {
			
		boolean[] res = new boolean[getSize()];
		
		for(int i = 0; i < getSize(); i++)
			res[i] = Integer.parseInt(getObject(col)[i].toString()) == 0 ? false : true;			
		
		return res;
		
	}
	
	public boolean getFirstBoolean(String col) {
		
		return getBoolean(col)[0];
		
	}
	
	public String[] getString(String col) {
				
		String[] res = new String[getSize()];
		
		for(int i = 0; i < getSize(); i++)
			if(col.matches("[^>]+>[^>]+"))
				res[i] = getJSONObject(col.split(">")[0])[i].get(col.split(">", 2)[1]).toString();
			else 
				res[i] = getObject(col)[i].toString();			
		
		return res;
		
	}
	
	public String getFirstString(String col) {
		
		return getString(col)[0];
		
	}
	
	public int[] getInt(String col) {
		
		int[] res = new int[getSize()];
		
		for(int i = 0; i < getSize(); i++)
			if(col.matches("[^>]+>[^>]+"))
				res[i] = Integer.parseInt(getJSONObject(col.split(">")[0])[i].get(col.split(">", 2)[1]).toString());
			else
				res[i] = Integer.parseInt(getObject(col)[i].toString());			
		
		return res;
		
	}
	
	public int getFirstInt(String col) {
		
		return getInt(col)[0];
		
	}
	
	public double[] getDouble(String col) {
		
		double[] res = new double[getSize()];
		
		for(int i = 0; i < getSize(); i++)
			if(col.matches("[^>]+>[^>]+"))
				res[i] = Double.parseDouble(getJSONObject(col.split(">")[0])[i].get(col.split(">", 2)[1]).toString());
			else 
				res[i] = Double.parseDouble(getObject(col)[i].toString());			
		
		return res;
		
	}
	
	public double getFirstDouble(String col) {
		
		return getDouble(col)[0];
		
	}
	
	public long[] getLong(String col) {
		
		long[] res = new long[getSize()];
		
		for(int i = 0; i < getSize(); i++)
			if(col.matches("[^>]+>[^>]+"))
				res[i] = Long.parseLong(getJSONObject(col.split(">")[0])[i].get(col.split(">", 2)[1]).toString());
			else 
				res[i] = Long.parseLong(getObject(col)[i].toString());			
		
		return res;
		
	}
	
	public long getFirstLong(String col) {
		
		return getLong(col)[0];
		
	}
	
	public Object[] getBlob(String col) {		
		
		Object[] res = new Object[getSize()];
		
		for(int i = 0; i < getSize(); i++)
			try {
				if(col.matches("[^>]+>[^>]+"))
					res[i] = Serializer.deserialize(getJSONObject(col.split(">")[0])[i].get(col.split(">", 2)[1]).toString());
				else 
					res[i] = Serializer.deserialize(getObject(col)[i].toString());			
			} catch(Exception e) {
				ConnectionManager.handleException(e, false);
			}
		
		return res;
	}
	
	public Object getFirstBlob(String col) {
		
		return getBlob(col)[0];
		
	}
	
	public JSONObject[] getJSONObject(String col) {
		
		JSONObject[] res = new JSONObject[getSize()];
		
		for(int i = 0; i < getSize(); i++)
			try {
				res[i] = (JSONObject) new JSONParser().parse(getObject(col)[i].toString());	
			} catch(Exception e) {}
		
		return res;
		
	}
	
	public JSONObject getFirstJSONObject(String col) {
		
		return getJSONObject(col)[0];
		
	}
	
	public ResultSet getAll() {
		
		return rs;
		
	}
	
	public int getSize() {
		
		return size;
		
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public Object[] getObjectFromType(Class<?> clazz) {
		
		if(resultsClasses.containsKey(clazz))
			return resultsClasses.get(clazz);
		
		Object[] ret = new Object[getSize()];

		if(!clazz.isAnnotationPresent(MySQLClass.class))
			return ret;
		
		boolean adjust = clazz.getAnnotation(MySQLClass.class).adjustFields();
		
		for(int i = 0; i < ret.length; i++) {
			
			try {
				Constructor<?> consttructor = clazz.getDeclaredConstructor();
				consttructor.setAccessible(true);
				ret[i] = consttructor.newInstance();
			} catch (InstantiationException | IllegalAccessException | NoSuchMethodException | SecurityException | IllegalArgumentException | InvocationTargetException e) {
				e.printStackTrace();
			}
		
			for(Field field : clazz.getDeclaredFields()) {
				
				if(field.isAnnotationPresent(Exclude.class) || Modifier.isStatic(field.getModifiers()))
					continue;				
				
				String fieldName = adjust ? ConventionFormatter.getMySQLName(field.getName()) : field.getName();
				
				if(ClassSerializer.hasSerializer(field.getType())) {
					
					ClassSerializer<?> serializer = ClassSerializer.getSerializer(field.getType());
					
					HashMap<String, Object> deserialize = new HashMap<String, Object>();
					
					HashMap<String, Class<?>> pattern = serializer.getPattern(fieldName);
					
					if(field.isAnnotationPresent(ExcludeField.class))
						for(String exField : field.getAnnotation(ExcludeField.class).fields())
							pattern.remove(exField);
					
					for(Entry<String, Class<?>> entry : pattern.entrySet())						
						deserialize.put(entry.getKey(), getObject(entry.getKey())[i]);					 
					
					try {
						field.setAccessible(true);
						field.set(ret[i], serializer.deserialize(fieldName, deserialize));
					} catch (IllegalArgumentException | IllegalAccessException e) {
						e.printStackTrace();
					}
					
				} else 
					try {
						field.setAccessible(true);
						
						if(field.getType().isEnum())							
							field.set(ret[i], getEnum((Class<Enum>) field.getType(), fieldName)[i]);			
						else if(Serializable.class.isAssignableFrom(field.getType()) && ColumnType.getType(field.getType()) == ColumnType.NONE)
							field.set(ret[i], getBlob(fieldName)[i]);
						else
							field.set(ret[i], getObject(fieldName)[i]);
						
					} catch (IllegalArgumentException | IllegalAccessException e) {
						e.printStackTrace();
					}					
			}
			
		}
		
		resultsClasses.put(clazz, ret);
		
		return ret;
		
	}
	
	public Object getFirstObjectFromType(Class<?> clazz) {
	
		return getObjectFromType(clazz)[0];
		
	}
	
	public <T extends Enum<T>> Object[] getEnum(Class<T> enumType, String col) {		
		
		Object[] ret = new Object[getSize()];
		
		for(int i = 0; i < getSize(); i++)
			if(col.matches("[^>]+>[^>]+"))
				ret[i] = Enum.valueOf(enumType, getJSONObject(col.split(">")[0])[i].get(col.split(">", 2)[1]).toString());
			else 
				ret[i] = Enum.valueOf(enumType, getObject(col)[i].toString());
				
		return ret;
		
	}
	
	@SuppressWarnings("unchecked")
	public <T extends Enum<T>> T getFirstEnum(Class<T> enumType, String col) {
		
		return (T) getEnum(enumType, col)[0];
		
	}
	
	public void close() {
		
		try {
			rs.close();
		} catch (SQLException e) {}
		
	}
	
	public boolean next() {
		
		index++;
		if(index == getSize())
			return false;
		else
			return true;
		
	}
	
	public Object getCurrentObject(String col) {
		
		return getObject(col)[index];
		
	}
	
	public boolean getCurrentBoolean(String col) {
			
		return getBoolean(col)[index];

	}
	
	public String getCurrentString(String col) {
			
		return getString(col)[index];
		
	}
	
	public int getCurrentInt(String col) {
		
		return getInt(col)[index];
		
	}
	
	public double getCurrentDouble(String col) {
		
		return getDouble(col)[index];
		
	}
	
	public long getCurrentLong(String col) {
		
		return getLong(col)[index];
		
	}
	
	public Object getCurrentBlob(String col) {		
		
		return getBlob(col)[index];
		
	}
	
	public Object getCurrentObjectFromType(Class<?> clazz) {
		
		return getObjectFromType(clazz)[index];
		
	}
	
	public JSONObject getCurrentJSONObject(String col) {
		
		return getJSONObject(col)[index];
		
	}
	
	@SuppressWarnings("unchecked")
	public <T extends Enum<T>> T getCurrentEnum(Class<T> enumType, String col) {
		
		return (T) getEnum(enumType, col)[index];
		
	}
	
}
