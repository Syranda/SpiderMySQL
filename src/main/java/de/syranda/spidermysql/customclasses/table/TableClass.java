package de.syranda.spidermysql.customclasses.table;

import de.syranda.spidermysql.customclasses.ConnectionManager;
import de.syranda.spidermysql.customclasses.MySQLTableSyntax;
import de.syranda.spidermysql.customclasses.builder.ForeignKey;
import de.syranda.spidermysql.customclasses.builder.Key;
import de.syranda.spidermysql.customclasses.builder.TableBuilder;

public class TableClass {
	
	private String tableName;
	private String database;
	
	private TableBuilder builder;
	
	public TableClass(String tableName) {
		
		this.tableName = tableName;
		
		builder = new TableBuilder(getTableName());

	}
	
	public TableClass(String tableName, String database) {

		this.tableName = tableName;
		this.database = database;
		
		builder = new TableBuilder(getTableName(), getDatabase());

	}
	
	public String getTableName() {
		
		return tableName;
		
	}
	
	public void insert(String values) {
		
		ConnectionManager.insertStatement(new MySQLTableSyntax(this).insert(values), false);
			
	}

	public void insertUpdate(String values) {
		
		ConnectionManager.insertStatement(new MySQLTableSyntax(this).insertUpdate(values), false);
			
	}
	
	public void insertIgnore(String values) {
		
		ConnectionManager.insertStatement(new MySQLTableSyntax(this).insertIgnore(values), false);
			
	}
	
	public void insert(Object clazz, String add) {
		
		ConnectionManager.insertStatement(new MySQLTableSyntax(this).insert(clazz, add), false);	
		
	}

	public void insert(Object clazz) { 
	
		ConnectionManager.insertStatement(new MySQLTableSyntax(this).insert(clazz), false);
		
	}
	
	public void insertUpdate(Object clazz, String add) {
		
		ConnectionManager.insertStatement(new MySQLTableSyntax(this).insertUpdate(clazz, add), false);
			
	}
	
	public void insertUpdate(Object clazz) {

		ConnectionManager.insertStatement(new MySQLTableSyntax(this).insertUpdate(clazz), false);
	}
	
	public void insertIgnore(Object clazz, String add) {
		
		ConnectionManager.insertStatement(new MySQLTableSyntax(this).insertIgnore(clazz, add), false);
		
	}
	
	public void insertIgnore(Object clazz) {

		ConnectionManager.insertStatement(new MySQLTableSyntax(this).insertIgnore(clazz), false);	
		
	}
	
	
	public RecordResult get(String what, String where) {
		
		return new RecordResult(ConnectionManager.resultStatement(new MySQLTableSyntax(this).get(what, where)));
		
	}
	
	public RecordResult get(String where) {
		
		return new RecordResult(ConnectionManager.resultStatement(new MySQLTableSyntax(this).get(where)));
		
	}
	
	public void update(String what, String where) {
		
		ConnectionManager.insertStatement(new MySQLTableSyntax(this).update(what, where), false);
		
	}
	
	public void remove(String where) {
		
		ConnectionManager.insertStatement(new MySQLTableSyntax(this).remove(where), false);
		
	}
	
	public RecordResult get() {
		
		return new RecordResult(ConnectionManager.resultStatement(new MySQLTableSyntax(this).get()));
		
	}
	
	public void clear() {
		
		ConnectionManager.insertStatement(new MySQLTableSyntax(this).clear(), true);
		
	}
	
	public TableBuilder builder() {
		
		return this.builder;
		
	}	
	
	public String getTarget() {

		return database == null ? "`" + tableName + "`" : "`" + database + "`.`" + tableName + "`";
		
	}
	
	public String getDatabase() {
		
		return this.database;
		
	}
	
	public void changeColumn(String field, MySQLField newField) {
		
		ConnectionManager.insertStatement(new MySQLTableSyntax(this).changeColumn(field, newField), true);
		
	}
	
	public void setColumnDefault(String field, Object newDefault) {
		
		ConnectionManager.insertStatement(new MySQLTableSyntax(this).setColumnDefault(field, newDefault), true);
		
	}
	
	public void changeColumnType(MySQLField field) {
		
		ConnectionManager.insertStatement(new MySQLTableSyntax(this).changeColumnType(field), true);
		
	}
	
	public void addField(MySQLField field) {
		
		ConnectionManager.insertStatement(new MySQLTableSyntax(this).addField(field), true);

	}

	public void removeColumn(String field) {
		
		ConnectionManager.insertStatement(new MySQLTableSyntax(this).removeColumn(field), true);
		
	}

	public void dropPrimaryKey() {
		
		ConnectionManager.insertStatement(new MySQLTableSyntax(this).dropPrimaryKey(), true);
		
	}
	
	public void dropKey(String key) {
		
		ConnectionManager.insertStatement(new MySQLTableSyntax(this).dropKey(key), true);
		
	}
	
	public void dropForeignKey(String foreignKey) {
		
		ConnectionManager.insertStatement(new MySQLTableSyntax(this).dropForeignKey(foreignKey), true);
		
	}
	
	public void addKey(Key key) {
		
		ConnectionManager.insertStatement(new MySQLTableSyntax(this).addKey(key), true);
		
	}
	
	public void addForeignKey(ForeignKey key) {
		
		ConnectionManager.insertStatement(new MySQLTableSyntax(this).addForeignKey(key), true);
		
	}
	
	public void delete() {
		
		ConnectionManager.insertStatement(new MySQLTableSyntax(this).delete(), true);
		
	}
	
}
