package de.syranda.spidermysql.customclasses.helper;

import de.syranda.spidermysql.customclasses.builder.Key;
import de.syranda.spidermysql.customclasses.builder.KeyType;

public class PrimaryKey extends Key{

	public PrimaryKey(String... columns) {
		super("", KeyType.PRIMARY, columns);
	}
	
}
