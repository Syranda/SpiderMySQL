package de.syranda.spidermysql.customclasses.helper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.EnchantmentStorageMeta;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.PotionMeta;
import org.bukkit.potion.PotionData;
import org.bukkit.potion.PotionType;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import de.syranda.spidermysql.customclasses.table.ClassSerializer;
import net.md_5.bungee.api.ChatColor;

public class DefaultClassSerializers {

	public static void loadDefaultClassSerializers() {
		
		ClassSerializer.addClassSerializer(Location.class, new ClassSerializer<Location>() {
			
			@Override
			public HashMap<String, Object> serialize(String fieldName, Location loc) {

				HashMap<String, Object> ret = new HashMap<String, Object>();
								
				ret.put(fieldName + "_x", loc.getX());
				ret.put(fieldName + "_y", loc.getY());
				ret.put(fieldName + "_z", loc.getZ());
				ret.put(fieldName + "_yaw", loc.getYaw());
				ret.put(fieldName + "_pitch", loc.getPitch());
				ret.put(fieldName + "_world", loc.getWorld().getName());
				
				return ret;

			}
			
			@Override
			public HashMap<String, Class<?>> getPattern(String fieldName) {
				
				HashMap<String, Class<?>> ret = new HashMap<String, Class<?>>();
				
				ret.put(fieldName + "_x", double.class);
				ret.put(fieldName + "_y", double.class);
				ret.put(fieldName + "_z", double.class);
				ret.put(fieldName + "_yaw", float.class);
				ret.put(fieldName + "_pitch", float.class);
				ret.put(fieldName + "_world", String.class);
				
				return ret;
				
			}
			
			@Override
			public Location deserialize(String fieldName, HashMap<String, Object> columns) {

				Location loc = new Location(Bukkit.getWorld(columns.get(fieldName + "_world").toString()), Double.parseDouble(columns.get(fieldName + "_x").toString()), Double.parseDouble(columns.get(fieldName + "_y").toString()), Double.parseDouble(columns.get(fieldName + "_z").toString()));
				
				if(columns.containsKey(fieldName + "_pitch"))
					loc.setPitch(Float.parseFloat(columns.get(fieldName + "_pitch").toString()));
				if(columns.containsKey(fieldName + "_yaw"))
					loc.setYaw(Float.parseFloat(columns.get(fieldName + "_yaw").toString()));
								
				return loc;
				
			}
		});
		
		ClassSerializer.addClassSerializer(JSONObject.class, new ClassSerializer<JSONObject>() {
			
			@Override
			public HashMap<String, Object> serialize(String fieldName, JSONObject object) {

				HashMap<String, Object> ret = new HashMap<String, Object>();
				
				ret.put(fieldName, object.toJSONString());
				
				return ret;
			}
			
			@Override
			public HashMap<String, Class<?>> getPattern(String fieldName) {

				HashMap<String, Class<?>> ret = new HashMap<String, Class<?>>();
				
				ret.put(fieldName, String.class);
				
				return ret;
			}
			
			@Override
			public JSONObject deserialize(String fieldName, HashMap<String, Object> columns) {
				
				try {
					return (JSONObject) new JSONParser().parse(columns.get(fieldName).toString());
				} catch (ParseException e) {
					return null;
				}
				
			}
		});
		
		ClassSerializer.addClassSerializer(ItemStack.class, new ClassSerializer<ItemStack>() {
			
			@SuppressWarnings({ "deprecation", "unchecked" })
			@Override
			public HashMap<String, Object> serialize(String fieldName, ItemStack is) {

				HashMap<String, Object> ret = new HashMap<String, Object>();
				ret.put(fieldName + "_material", is.getType().name());
				ret.put(fieldName + "_amount", is.getAmount());
				ret.put(fieldName + "_data", is.getData().getData());
				
				if(is.hasItemMeta()) {
				
					ItemMeta im = is.getItemMeta();
					
					ret.put(fieldName + "_displayname", im.hasDisplayName() ? im.getDisplayName().replace("§", "&") : null);
					
					String lore = null;
					
					if(im.hasLore()) {
						
						lore = "";
						
						for(int i = 0; i < im.getLore().size(); i++)
							lore += i == 0 ? im.getLore().get(i).replace("§", "&") : "\\n" + im.getLore().get(i).replace("§", "&");
							
					}
					
					ret.put(fieldName + "_lore", lore);
					
					String ench = null;
					
					if(im.hasEnchants()) {
						
						ench = "";
						
						if(is.getType() == Material.ENCHANTED_BOOK) {
							
							EnchantmentStorageMeta esm = (EnchantmentStorageMeta) im;
							
							for(int i = 0; i < esm.getStoredEnchants().size(); i++) {
								
								Entry<Enchantment, Integer> entry = (Entry<Enchantment, Integer>) esm.getStoredEnchants().entrySet().toArray()[i];
								
								ench += i == 0 ? entry.getKey().getName() + ":" + entry.getValue() : ";" + entry.getKey() + ":" + entry.getValue();
								
							}
							
						} else 							
							for(int i = 0; i < im.getEnchants().size(); i++) {
								
								Entry<Enchantment, Integer> entry = (Entry<Enchantment, Integer>) im.getEnchants().entrySet().toArray()[i];
								
								ench += i == 0 ? entry.getKey().getName() + ":" + entry.getValue() : ";" + entry.getKey() + ":" + entry.getValue();
								
							}
													
					}
					
					ret.put(fieldName + "_enchantments", ench);			

					String flags = null;
					
					if(!im.getItemFlags().isEmpty()) {
						
						flags = "";
						
						for(int i = 0; i < im.getItemFlags().size(); i++)
							flags += i == 0 ? ((ItemFlag) im.getItemFlags().toArray()[i]).name() : ";" + ((ItemFlag) im.getItemFlags().toArray()[i]).name();
						
					}
					
					ret.put(fieldName + "_flags", flags);
					
					String potioneffects = null;
					
					if(im instanceof PotionMeta) {
						
						PotionMeta pm = (PotionMeta) im;
						
						potioneffects = pm.getBasePotionData() + ":" + pm.getBasePotionData().isExtended() + ":" + pm.getBasePotionData().isUpgraded();
						
					}
					
					ret.put(fieldName + "_potioneffects", potioneffects);
					
				}
							
				return ret;
				
			}
			
			@Override
			public HashMap<String, Class<?>> getPattern(String fieldName) {

				HashMap<String, Class<?>> ret = new HashMap<String, Class<?>>();
				
				ret.put(fieldName + "_material", String.class);
				ret.put(fieldName + "_amount", int.class);
				ret.put(fieldName + "_data", int.class);
				
				ret.put(fieldName + "_displayname", String.class);
				ret.put(fieldName + "_lore", String.class);
				ret.put(fieldName + "_enchantments", String.class);
				ret.put(fieldName + "_potioneffects", String.class);
				ret.put(fieldName + "_flags", String.class);
				
				return ret;
				
			}
			
			@Override
			public ItemStack deserialize(String fieldName, HashMap<String, Object> columns) {

				ItemStack is = new ItemStack(Material.valueOf(columns.get(fieldName + "_material").toString()));
				
				int amount = columns.containsKey(fieldName + "_amount") ? Integer.parseInt(columns.get(fieldName + "_amount").toString()) : 1;
				
				is.setAmount(amount);
				
				short data = columns.containsKey(fieldName + "_data") ? Short.parseShort(columns.get(fieldName + "_data").toString()) : 0;
				
				is.setDurability(data);
				
				ItemMeta im = is.getItemMeta();
				
				String displayname = columns.containsKey(fieldName + "_displayname") ? ChatColor.translateAlternateColorCodes('&', columns.get(fieldName + "_displayname").toString()) : null;
				
				im.setDisplayName(displayname);
				
				if(columns.containsKey(fieldName + "_lore") && columns.get(fieldName + "_lore") != null) {
					
					List<String> lore = new ArrayList<String>();
					
					for(String loreL : columns.get(fieldName + "_lore").toString().split("\\\\n"))
						if(!loreL.matches("^(\\s)*$"))
							lore.add(ChatColor.translateAlternateColorCodes('&', loreL));
					
					im.setLore(lore);					
					
				}
				
				if(columns.containsKey(fieldName + "_enchantments") && columns.get(fieldName + "_enchantments") != null) 					
					for(String enchString : columns.get(fieldName + "_enchantments").toString().split(";")) {
						
						Enchantment ench = Enchantment.getByName(enchString.split(":")[0]);
						int level = Integer.parseInt(enchString.split(":")[1]);
						
						if(is.getType() == Material.ENCHANTED_BOOK)
							((EnchantmentStorageMeta) im).addStoredEnchant(ench, level, true);
						else
							im.addEnchant(ench, level, true);
						
					}
				
				if(columns.containsKey(fieldName + "_flags") && columns.get(fieldName + "_flags") != null) 					
					for(String flagString : columns.get(fieldName + "_flags").toString().split(";"))
						im.addItemFlags(ItemFlag.valueOf(flagString));
				
				
				if(columns.containsKey(fieldName + "_potioneffects") && columns.get(fieldName + "_potioneffects") != null) {

					String potioneffect = columns.get(fieldName + "_potioneffects").toString();
					
					((PotionMeta) im).setBasePotionData(new PotionData(PotionType.valueOf(potioneffect.split(";")[0]), Boolean.parseBoolean(potioneffect.split(";")[1]), Boolean.parseBoolean(potioneffect.split(";")[2])));
					
				}
				
				is.setItemMeta(im);
				
				return is;
				
			}
		});
		
	}
	
}
