package de.syranda.spidermysql.customclasses.helper;

import de.syranda.spidermysql.customclasses.builder.ColumnType;
import de.syranda.spidermysql.customclasses.table.MySQLField;

public class IdField extends MySQLField {

	public IdField() {
		super("id", ColumnType.INT);
		
		setAutoIncrement(true);
		setPrimaryKey(true);
		
	}
	
}
