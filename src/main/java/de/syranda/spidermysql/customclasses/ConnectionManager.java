package de.syranda.spidermysql.customclasses;

import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;

import com.mysql.jdbc.Connection;
import com.mysql.jdbc.PreparedStatement;
import com.mysql.jdbc.Statement;

import de.syranda.spidermysql.plugin.ConfigValues;

public class ConnectionManager {

	private static Connection conn;
	private static HashMap<String, Connection> backupConnections = new HashMap<String, Connection>();
	private static SpiderExceptionHandler handler = new SpiderExceptionHandler() {
		
		@Override
		public void handleException(Exception exception, boolean queryExecuted) {

			exception.printStackTrace();
			
		}		
		
	};
	
	public static void handleException(Exception exception, boolean queryExecuted) {
		
		handler.handleException(exception, queryExecuted);
		
	}
	
	public static void setExceptionHandler(SpiderExceptionHandler handler) {
		
		ConnectionManager.handler = handler;
		
	}
	
	public static boolean connect(String host, int port, String database, String user, String password) {
		
		try {
			conn = (Connection) DriverManager.getConnection("jdbc:mysql://" + host + ":" + port + "/" + database, user, password);					
		} catch (SQLException e) {
			System.err.println("[SpiderMySQL] Error connecting to MySQL-Server (" + e.getMessage() + ")");	
			return false;
		}
		conn.setTcpKeepAlive(true);
		conn.setAutoReconnect(true);
		
		return true;
		
	}
	
	public static boolean addBackupConnection(String host, int port, String database, String user, String password) {
		
		Connection backup;
		
		try {
			backup = (Connection) DriverManager.getConnection("jdbc:mysql://" + host + ":" + port + "/" + database, user, password);					
		} catch (SQLException e) {
			System.err.println("[SpiderMySQL] Error connecting to MySQL-Server (Backup) (" + e.getMessage() + ")");	
			return false;
		}
		backup.setTcpKeepAlive(true);
		backup.setAutoReconnect(true);
		
		backupConnections.put(database, backup);
		
		return true;
		
	}
	
	public static void insertStatement(String query, boolean writeToBackUp) {
		
		if(ConfigValues.LOG_QUERY)
			System.out.println(query);
		
		try {
			PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query);
			ps.executeUpdate();
			ps.close();			
				
			for(Connection back : backupConnections.values()) {
				
				if(back.getHost().equals(conn.getHost()) && !writeToBackUp)
					continue;
				
				PreparedStatement backPs = (PreparedStatement) back.prepareStatement(query);
				backPs.executeUpdate();
				backPs.close();
				
			}
						
		} catch (SQLException e) {	
			handleException(e, true);
		}
		
	}
	
	public static ResultSet resultStatement(String query) {
		
		if(ConfigValues.LOG_QUERY)
			System.out.println(query);
		
		try {
			
			Statement s = (Statement) conn.createStatement();
			return s.executeQuery(query);
			
		} catch (SQLException e) {
			handleException(e, true);
			return null;
		}
		
	}

	public static void disconnect() {

		try {
			conn.close();
			
			for(Connection connection : backupConnections.values())
				connection.close();
			
		} catch (SQLException e) {}
		
	}
	
	public static HashMap<String, Connection> getBackupConnections() {
		
		return backupConnections;
		
	}
	
}
