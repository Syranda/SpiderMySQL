package de.syranda.spidermysql.customclasses;

public interface SpiderExceptionHandler {
	
	public void handleException(Exception exception, boolean queryExecuted);

}
